package pl.org.opi.ej.solid;

import pl.org.opi.ej.solid.employees.*;

public class SolidMain {
      
    public static void main(String[] args){

        Director director = new Director("Barbara", "Barbarowska", 10);
        Manager manager = new Manager("Agata", "Agatowa", 8);

        Specialist specialist = new Specialist("Jan", "Kowalski", 5);
        specialist.doMagic();

        Accountant accountant = new Accountant("Adam", "Nowak", 3);
        accountant.doAccounting();

        String issue = "SYSTEM FAILURE";
        IssueHandler issueHandler = new Intern();
        IssueHandler issueHandler2 = new Intern();
        issueHandler.handleIssue(issue, issueHandler2);


        Management management = director;
        management.sendMessage("yearly assessment time!");

        manager.performAssessment(specialist);
        manager.performAssessment(accountant);

        System.out.println("specialist = " + specialist);
        System.out.println("accountant = " + accountant);
        
    }
    
}
