package pl.org.opi.ej.solid.employees;

import pl.org.opi.ej.solid.IssueHandler;

public class Intern implements IssueHandler {

    @Override
    public void handleIssue(String issue, IssueHandler issueHandler) {
        System.out.println("intern is handling issue " + issue);
    }
}
