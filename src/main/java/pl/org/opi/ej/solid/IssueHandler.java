package pl.org.opi.ej.solid;


@FunctionalInterface
public interface IssueHandler {

    void handleIssue(String issue, IssueHandler issueHandler);

}
