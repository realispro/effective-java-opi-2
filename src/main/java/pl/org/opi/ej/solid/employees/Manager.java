package pl.org.opi.ej.solid.employees;

import pl.org.opi.ej.solid.Employee;
import pl.org.opi.ej.solid.Management;

import java.util.Date;
import java.util.Random;

public class Manager extends Employee implements Management {

    public Manager(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void performAssessment(Employee employee){
        int rate = new Random(new Date().getTime()).nextInt(10)+1;
        if(rate>5){
            employee.setGrade(employee.getGrade()+1);
        }
    }

}
