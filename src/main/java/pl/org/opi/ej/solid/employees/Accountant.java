package pl.org.opi.ej.solid.employees;

import pl.org.opi.ej.solid.Employee;

public class Accountant extends Employee {


    public Accountant(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void doAccounting(){
        System.out.println("accountant is accounting");
    }


}
