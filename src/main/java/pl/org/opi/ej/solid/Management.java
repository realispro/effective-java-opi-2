package pl.org.opi.ej.solid;

public interface Management {

    default void sendMessage(String message) {
        System.out.println("[MESSAGE] " + message);
    }
}
