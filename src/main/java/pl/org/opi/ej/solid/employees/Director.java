package pl.org.opi.ej.solid.employees;

import pl.org.opi.ej.solid.Employee;
import pl.org.opi.ej.solid.Management;

public class Director extends Employee implements Management {
    public Director(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }
}
