package pl.org.opi.ej.gof.structural.adapter.pressure.bar;

import pl.org.opi.ej.gof.structural.adapter.pressure.PascalPressure;

import java.util.Random;

// Adaptee
public class BarPressureProvider {

    public float getPressure(){
        return new Random().nextFloat(); // 0.0-1.0
    }

}
