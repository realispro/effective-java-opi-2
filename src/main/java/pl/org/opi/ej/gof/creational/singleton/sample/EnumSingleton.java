package pl.org.opi.ej.gof.creational.singleton.sample;

public enum EnumSingleton {

    INSTANCE;

    private int version = 1;

    /*private EnumSingleton(){}*/

    public static EnumSingleton getInstance(){
        return INSTANCE;
    }

    public void doNothing(){
        System.out.println("doing nothing " + version);
        version++;
    }

}
