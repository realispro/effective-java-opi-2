package pl.org.opi.ej.gof.behavioral.observer.press;

public interface NewsListener {
    void pushNews(News news);
}
