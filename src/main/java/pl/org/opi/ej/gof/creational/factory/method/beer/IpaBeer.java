package pl.org.opi.ej.gof.creational.factory.method.beer;

public class IpaBeer implements Beer {
    @Override
    public String getName() {
        return "ipa beer";
    }
    @Override
    public float getVoltage() {
        return 4;
    }
}
