package pl.org.opi.ej.gof.behavioral.observer.press;

import java.util.Objects;

public class Subscriber implements NewsListener {

    private int sId;

    public Subscriber(int sId) {
        this.sId = sId;
    }

    @Override
    public void pushNews(News news) {
        System.out.println("sId: " + sId + " received news: " + news);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subscriber)) return false;
        Subscriber that = (Subscriber) o;
        return sId == that.sId;
    }
    @Override
    public int hashCode() {
        return Objects.hash(sId);
    }
}
