package pl.org.opi.ej.gof.creational.factory.method.notification;

public class NotificationFactory {

    public static Notification createNotification(){
        return createNotification(NotificationType.MOBILE);
    }

    public static Notification createNotification(NotificationType type){
        switch (type){
            case TRADITIONAL:
                return new PigeonNotification();
            default:
                return new SMSNotification();
        }
    }
}
