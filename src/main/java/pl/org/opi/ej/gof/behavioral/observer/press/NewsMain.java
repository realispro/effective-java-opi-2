package pl.org.opi.ej.gof.behavioral.observer.press;

public class NewsMain {

    public static void main(String[] args) {

        PressOffice po = new PressOffice();

        Subscriber subscriber1 = new Subscriber(1);
        Subscriber subscriber2 = new Subscriber(2);
        Subscriber subscriber3 = new Subscriber(3);
        Subscriber subscriber4 = new Subscriber(4);

        po.addSubscriber(subscriber1);
        po.addSubscriber(subscriber2);
        po.addSubscriber(subscriber3);
        po.addSubscriber(subscriber4);

        po.dayWork();
    }
}
