package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.predator;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.AnimalProvider;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Bird;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Fish;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Mammal;

public class PredatorAnimalProvider implements AnimalProvider {
    @Override
    public Bird provideBird() {
        return new Eagle();
    }

    @Override
    public Fish provideFish() {
        return new Shark();
    }

    @Override
    public Mammal provideMammal() {
        return new Tiger();
    }
}
