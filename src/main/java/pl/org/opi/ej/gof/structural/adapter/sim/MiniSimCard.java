package pl.org.opi.ej.gof.structural.adapter.sim;

// Adaptee
public class MiniSimCard {

    private byte[] id;

    public MiniSimCard(String id) {
        this.id = id.getBytes();
    }

    public byte[] getId() {
        return id;
    }
}
