package pl.org.opi.ej.gof.creational.builder.task;

public class TaskBuilder {

    private final Task task = new Task();

    public TaskBuilder setId(int taskId) {
        task.setId(taskId);
        return this;
    }

    public TaskBuilder setDescription(String description) {
        if (task.getTitle() == null) {
            throw new IllegalStateException("Set Title first.");
        }
        task.setDescription(description);
        return this;
    }

    public TaskBuilder setTitle(String title) {
        if (task.getId() == 0) {
            throw new IllegalStateException("Set id first.");
        }
        task.setTitle(title);
        return this;
    }

    public TaskBuilder setPriority(int priority) {
        if (priority < 1 || priority > 3) {
            throw new IllegalStateException("Set priority 1-3.");
        }
        task.setPriority(priority);
        return this;
    }


    public Task buildTask() {
        if (task.getId() == 0) {
            throw new IllegalStateException("no id is set.");
        } else if (task.getTitle() == null) {
            throw new IllegalStateException("no title is set.");
        } else if (task.getDescription() == null) {
            throw new IllegalStateException("no description is set");
        }
        return task;
    }
}
