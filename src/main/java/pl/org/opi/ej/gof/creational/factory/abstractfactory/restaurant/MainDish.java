package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant;

public interface MainDish {

    void eat();
}
