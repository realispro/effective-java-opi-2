package pl.org.opi.ej.gof.behavioral.strategy.encrypt;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

// Context
public final class SafeStore {

    private Encrypter encrypter;

    public SafeStore(Encrypter encrypter) {
        this.encrypter = encrypter;
    }

    public boolean store(Object o){
        try {
            byte[] toStore = encrypter.encrypt(o.toString());
            storeEncrypted(toStore);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private final void storeEncrypted(byte[] bytes){

        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("encrypter-safe.store"))){
            bos.write(bytes);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
