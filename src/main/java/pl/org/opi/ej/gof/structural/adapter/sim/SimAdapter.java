package pl.org.opi.ej.gof.structural.adapter.sim;

public class SimAdapter implements Sim {

    private final MiniSimCard card;

    public SimAdapter(MiniSimCard card) {
        this.card = card;
    }

    @Override
    public String getId() {
        return new String(card.getId());
    }
}
