package pl.org.opi.ej.gof.structural.flyweight.particles;

// immutable
public class ParticleCaliber {

    private final int caliber;

    private final String icon;

    public ParticleCaliber(int caliber) {
        this.caliber = caliber;
        this.icon = caliber + ".jpg"; // loading image bytes
    }

    public int getCaliber() {
        return caliber;
    }

    public String getIcon() {
        return icon;
    }
}
