package pl.org.opi.ej.gof.structural.decorator.email;

public class RODODecorator extends AddOnSignature {

    private static final String RODO_CLAUSE = "RODO clause etcetera ....";

    public RODODecorator(Email email) {
        super(email, RODO_CLAUSE);
    }
}
