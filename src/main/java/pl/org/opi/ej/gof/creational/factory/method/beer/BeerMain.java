package pl.org.opi.ej.gof.creational.factory.method.beer;

public class BeerMain {

    public static void main(String[] args) {

        BeerFactory bf = new BeerFactory();
        Beer beer = bf.createBeer(BeerType.PILS);

        System.out.println("drinking beer " + beer.getName() + " of voltage " + beer.getVoltage());

    }
}
