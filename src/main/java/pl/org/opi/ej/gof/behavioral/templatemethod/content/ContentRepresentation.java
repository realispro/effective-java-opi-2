package pl.org.opi.ej.gof.behavioral.templatemethod.content;

public class ContentRepresentation {

    protected String content;

    public ContentRepresentation(String content) {
        this.content = content;
    }

    protected final String representContent(){
        return "{" + content + "}";
    }


}
