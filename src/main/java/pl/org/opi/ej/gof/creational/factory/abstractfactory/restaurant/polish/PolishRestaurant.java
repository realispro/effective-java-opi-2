package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class PolishRestaurant implements Restaurant {
    @Override
    public MainDish getMainDish() {
        return new Schabowy();
    }
    @Override
    public Dessert getDessert() {
        return new Faworki();
    }
}
