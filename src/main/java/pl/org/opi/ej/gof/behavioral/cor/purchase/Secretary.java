package pl.org.opi.ej.gof.behavioral.cor.purchase;

public class Secretary extends Approver {

    public Secretary(String name) {
        super(name);
    }

    @Override
    public int getLimit() {
        return 500;
    }


}