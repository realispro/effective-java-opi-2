package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.RestaurantProvider;

public class PolishRestaurantProvider implements RestaurantProvider {
    @Override
    public Restaurant getRestaurant() {
        return new PolishRestaurant();
    }
}
