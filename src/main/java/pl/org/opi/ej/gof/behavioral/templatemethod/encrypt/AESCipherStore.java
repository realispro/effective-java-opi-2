package pl.org.opi.ej.gof.behavioral.templatemethod.encrypt;

public class AESCipherStore extends CipherStore{

    private byte[] keyValue = {
            '1', '1', '3', '4', '5', '6', '7', '8',
            '1', '2', '3', '4', '5', '6', '7', '8'
    };

    @Override
    public String getAlgorithm() {
        return "AES";
    }

    @Override
    public byte[] getKeyValue() {
        return keyValue;
    }
}
