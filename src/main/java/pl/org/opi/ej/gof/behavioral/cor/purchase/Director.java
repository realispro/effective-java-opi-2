package pl.org.opi.ej.gof.behavioral.cor.purchase;

public class Director extends Approver {

    public Director(String name) {
        super(name);
    }

    @Override
    public int getLimit() {
        return 10000;
    }


}
