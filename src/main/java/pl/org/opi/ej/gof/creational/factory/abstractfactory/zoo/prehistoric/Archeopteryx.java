package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Bird;

public class Archeopteryx implements Bird {
    @Override
    public void fly() {
        System.out.println("archeopteryx is flying");
    }
}
