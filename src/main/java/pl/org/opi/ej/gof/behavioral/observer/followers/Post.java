package pl.org.opi.ej.gof.behavioral.observer.followers;

public class Post {

    private String message;

    public Post(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
