package pl.org.opi.ej.gof.behavioral.cor.purchase;

public class President extends Approver {

    public President(String name) {
        super(name);
    }

    @Override
    public int getLimit() {
        return -1;
    }
}
