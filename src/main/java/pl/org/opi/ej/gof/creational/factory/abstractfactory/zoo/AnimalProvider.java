package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.predator.PredatorAnimalProvider;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.prehistoric.PrehistoricAnimalProvider;

public interface AnimalProvider {

    Bird provideBird();

    Fish provideFish();

    Mammal provideMammal();

    static AnimalProvider getInstance(AnimalType type){
        switch (type){
            case PREDATOR:
                return new PredatorAnimalProvider();
            default:
                return new PrehistoricAnimalProvider();
        }
    }

}
