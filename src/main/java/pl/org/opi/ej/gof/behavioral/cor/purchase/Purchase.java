package pl.org.opi.ej.gof.behavioral.cor.purchase;

//  Request
public class Purchase {

    private int amount;

    private String purpose;

    private boolean approved = false;

    private String approvedBy;

    public Purchase(int amount, String purpose) {
        this.amount = amount;
        this.purpose = purpose;
    }

    public int getAmount() {
        return amount;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void approve(String by) {
        this.approved = true;
        this.approvedBy = by;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "amount=" + amount +
                ", purpose='" + purpose + '\'' +
                ", approved=" + approved +
                ", approvedBy='" + approvedBy + '\'' +
                '}';
    }
}
