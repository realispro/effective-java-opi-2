package pl.org.opi.ej.gof.structural.flyweight.particles;

public class GameMain {

    public static void main(String[] args) {

        Unit u1 = new Unit("u1", 15,2);
        Unit u2 = new Unit("u2",14,3);
        Unit u3 = new Unit("u3",14,4);

        u1.fire(u2);
        u2.fire(u3);
        u3.fire(u2);
        u2.fire(u1);
        u1.fire(u2);
        u2.fire(u3);
        u3.fire(u2);
        u2.fire(u1);
        u1.fire(u2);
        u2.fire(u3);
        u3.fire(u2);
        u2.fire(u1);
    }
}
