package pl.org.opi.ej.gof.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

public class CoinCache {

    private static Map<Currency, Map<Integer,Coin>> cache = new HashMap<>();

    public static Coin getCoin(int value, Currency currency) {

        Map<Integer, Coin> values = cache.get(currency);
        if(values==null){
            values = new HashMap<>();
            cache.put(currency, values);
        }

        Coin coin = values.get(value);
        if (coin == null) {
            coin = new Coin(value, currency);
            values.put(value,coin);
        }
        return coin;
    }
}
