package pl.org.opi.ej.gof.structural.adapter.pressure;

// Target
public interface PascalPressure {

    float getPressure();
}
