package pl.org.opi.ej.gof.structural.decorator.sandwich;

public class AddonDecorator implements Sandwich{

    private Sandwich sandwich;

    private String addon;

    public AddonDecorator(Sandwich sandwich, String addon) {
        this.sandwich = sandwich;
        this.addon = addon;
    }

    @Override
    public String content() {
        return sandwich.content() + ", " + addon;
    }
}
