package pl.org.opi.ej.gof.structural.decorator.sandwich;

public interface Sandwich {

    String content();
}
