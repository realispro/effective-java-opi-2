package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Pasta implements MainDish {
    @Override
    public void eat() {
        System.out.println("eating pasta");
    }
}
