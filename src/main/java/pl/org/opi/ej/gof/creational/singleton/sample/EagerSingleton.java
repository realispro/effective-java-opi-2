package pl.org.opi.ej.gof.creational.singleton.sample;

public class EagerSingleton {

    private static EagerSingleton instance;

    static {
        try {
            instance = new EagerSingleton();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private EagerSingleton() throws Exception{}

    public static EagerSingleton getInstance(){
        return instance;
    }

    public void doNothing(){
        System.out.println("doing nothing");
    }


}
