package pl.org.opi.ej.gof.structural.decorator.sandwich;

public class ButterDecorator implements Sandwich{

    private Sandwich sandwich;

    public ButterDecorator(Sandwich sandwich) {
        this.sandwich = sandwich;
    }

    @Override
    public String content() {
        return sandwich.content() + ", butter";
    }
}
