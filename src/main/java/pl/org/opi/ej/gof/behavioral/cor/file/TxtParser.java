package pl.org.opi.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TxtParser extends Parser {

    @Override
    public void process(File file) throws IOException {
        Files
                .lines(Paths.get(file.getAbsolutePath()))
                .forEach(System.out::println);
    }

    @Override
    public String getTypeFile() {
        return ".txt";
    }
}
