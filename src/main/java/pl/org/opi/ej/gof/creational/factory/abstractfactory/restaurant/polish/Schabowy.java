package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Schabowy implements MainDish {
    @Override
    public void eat() {
        System.out.println("eating schabowy");
    }
}
