package pl.org.opi.ej.gof.creational.builder.pizza;

public class PizzaMain {

    public static void main(String[] args) {
        System.out.println("let's eat some pizza!");

        Pizza pizza = new PizzaBuilder()
                .setDough("grube")
                .addTopping("szynka")
                .addTopping("kukurydza")
                .setSause("ketchup")
                .bake();
                /*new Pizza();
        pizza.setDough("cienkie");
        pizza.addTopping("salami");
        pizza.addTopping("pomidor");
        pizza.addTopping("ser");
        pizza.setSause("czosnkowy");*/

        System.out.println("eating pizza " + pizza);
    }

}
