package pl.org.opi.ej.gof.behavioral.observer.followers;

import java.util.ArrayList;
import java.util.List;

// Subject + Observer
public class Profile implements Follower {

    private List<Follower> followers = new ArrayList<>();

    private String name;

    public Profile(String name) {
        this.name = name;
    }

    // Subject
    public void publish(String message) {
        Post post = new Post(name + ": " + message);
        System.out.println("[" + name + " published] " + post.getMessage());

        followers.forEach(f->f.follow(post));
    }

    public void register(Follower follower){
        followers.add(follower);
    }


    // Observer
    @Override
    public void follow(Post post) {
        System.out.println("[" + name + " received ] " + post.getMessage());
        publish(post.getMessage());
    }
}
