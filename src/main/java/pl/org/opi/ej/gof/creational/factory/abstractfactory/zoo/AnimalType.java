package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo;

public enum AnimalType {

    PREHISTORIC,
    PREDATOR
}
