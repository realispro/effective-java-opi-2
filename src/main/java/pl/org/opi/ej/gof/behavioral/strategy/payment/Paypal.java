package pl.org.opi.ej.gof.behavioral.strategy.payment;

public class Paypal implements PaymentMethod{

    private String accountName;

    private String password;

    public Paypal(String accountName, String password) {
        this.accountName = accountName;
        this.password = password;
    }

    public void chargePaypal(double value) {
        System.out.println("connecting to paypal service, account name: " + accountName);
    }

    @Override
    public void chargePay(double value) {
        chargePaypal(value);
    }
}
