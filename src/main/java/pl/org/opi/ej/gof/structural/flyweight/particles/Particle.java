package pl.org.opi.ej.gof.structural.flyweight.particles;

public class Particle {

    private int coordX;

    private int coordY;

    private double vector;

    private ParticleCaliber particleCaliber;

    public Particle(int coordX, int coordY, double vector, int caliber) {
        this.coordX = coordX;
        this.coordY = coordY;
        this.vector = vector;
        this.particleCaliber = ParticleCaliberCache.getCaliber(caliber);
                //new ParticleCaliber(caliber);
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }


    public double getVector() {
        return vector;
    }

    public void setVector(double vector) {
        this.vector = vector;
    }

    public int getCaliber() {
        return particleCaliber.getCaliber();
    }

    public String getIcon() {
        return particleCaliber.getIcon();
    }

}
