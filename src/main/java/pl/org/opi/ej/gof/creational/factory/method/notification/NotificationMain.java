package pl.org.opi.ej.gof.creational.factory.method.notification;

public class NotificationMain {

    public static void main(String[] args) {

        NotificationFactory nf = new NotificationFactory();
        Notification notification = NotificationFactory.createNotification();
        notification.emit("Poland lost with Italy :(");

    }
}
