package pl.org.opi.ej.gof.structural.decorator.email;

public class UrgencyDecorator implements Email{

    private Email email;

    public UrgencyDecorator(Email email) {
        this.email = email;
    }

    @Override
    public String getTitle() {
        return "URGENT: " + email.getTitle();
    }

    @Override
    public String getContent() {
        return email.getContent();
    }
}
