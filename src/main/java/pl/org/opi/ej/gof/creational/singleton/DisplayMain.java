package pl.org.opi.ej.gof.creational.singleton;

public class DisplayMain {

    public static void main(String[] args) {

        Display display1 = Display.getInstance();
        Display display2 = Display.getInstance();

        display1.show("text from display1");
        display2.show("text from display2");
        System.out.println("same? " + (display1==display2));

    }
}
