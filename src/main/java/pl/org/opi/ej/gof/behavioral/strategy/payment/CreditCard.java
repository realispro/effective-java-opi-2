package pl.org.opi.ej.gof.behavioral.strategy.payment;

public class CreditCard implements PaymentMethod{

    private String number;

    private int ccv;

    public CreditCard(String number, int ccv) {
        this.number = number;
        this.ccv = ccv;
    }

    public void chargeCard(double value) {
        System.out.println("charging card " + number);
    }

    @Override
    public void chargePay(double value) {
        chargeCard(value);
    }
}
