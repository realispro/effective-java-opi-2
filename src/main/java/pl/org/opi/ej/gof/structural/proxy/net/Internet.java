package pl.org.opi.ej.gof.structural.proxy.net;

public interface Internet {


    void connectTo(String address);
}
