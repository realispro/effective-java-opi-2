package pl.org.opi.ej.gof.structural.adapter.pressure.bar;

import pl.org.opi.ej.gof.structural.adapter.pressure.PascalPressure;

// Adapter
public class Bar2PascalAdapter implements PascalPressure {

    private BarPressureProvider barPressureProvider;

    public Bar2PascalAdapter(BarPressureProvider barPressureProvider) {
        this.barPressureProvider = barPressureProvider;
    }

    @Override
    public float getPressure() {
        return barPressureProvider.getPressure() * 100_000;
    }
}
