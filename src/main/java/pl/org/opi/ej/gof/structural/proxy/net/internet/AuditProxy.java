package pl.org.opi.ej.gof.structural.proxy.net.internet;

import pl.org.opi.ej.gof.structural.proxy.net.Internet;

public class AuditProxy implements Internet {

    private Internet internet;

    public AuditProxy(Internet internet) {
        this.internet = internet;
    }

    @Override
    public void connectTo(String address) {
        System.out.println("[audit log] " + address);
        internet.connectTo(address);
    }
}
