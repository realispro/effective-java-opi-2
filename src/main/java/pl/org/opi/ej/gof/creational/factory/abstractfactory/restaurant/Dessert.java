package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant;

public interface Dessert {

    void enjoy();
}
