package pl.org.opi.ej.gof.structural.adapter.sim;

public class SimMain {

    public static void main(String[] args) {
        System.out.println("let's install sim card");

        MiniSimCard miniSim = new MiniSimCard("12345");
        Device d = new Device();
        Sim sim = new SimAdapter(miniSim);
        d.install(sim);

    }
}
