package pl.org.opi.ej.gof.structural.proxy.net.internet;

import pl.org.opi.ej.gof.structural.proxy.net.Internet;

public class InternetProvider {

    public static Internet provideInternet(){
        return new NetProxy(new AuditProxy(new RealNetwork()));
    }
}
