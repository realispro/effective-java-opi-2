package pl.org.opi.ej.gof.structural.decorator.sandwich;

public class SandwichMain {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich =
                new AddonDecorator(
                        new AddonDecorator(
                                new ButterDecorator(
                                        new WhiteBread()
                                ),
                                "cheese"),
                        "tomato");

        System.out.println("eating " + sandwich.content());
    }
}
