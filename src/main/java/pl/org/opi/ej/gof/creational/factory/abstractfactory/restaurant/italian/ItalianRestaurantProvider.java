package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.RestaurantProvider;

public class ItalianRestaurantProvider implements RestaurantProvider {
    @Override
    public Restaurant getRestaurant() {
        return new ItalianRestaurant();
    }
}
