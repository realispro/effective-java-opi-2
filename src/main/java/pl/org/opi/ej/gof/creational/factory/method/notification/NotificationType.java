package pl.org.opi.ej.gof.creational.factory.method.notification;

public enum NotificationType {

    MOBILE,
    TRADITIONAL,
    EMAIL;
}
