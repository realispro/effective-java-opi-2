package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class ItalianRestaurant implements Restaurant {
    @Override
    public MainDish getMainDish() {
        return new Pasta();
    }
    @Override
    public Dessert getDessert() {
        return new Pandoro();
    }
}
