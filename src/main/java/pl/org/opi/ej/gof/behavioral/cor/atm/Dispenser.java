package pl.org.opi.ej.gof.behavioral.cor.atm;

public class Dispenser {

    private Dispenser next;

    private int value;

    public Dispenser(int value) {
        this.value = value;
    }

    public void dispense(Request request){
        int amount = request.getAmount() / value;
        int rest = request.getAmount() % value;
        System.out.println("dispensing " + amount + " notes of " + value + "$");
        if(rest>0){
            request.setAmount(rest);
            if(next!=null){
                next.dispense(request);
            } else {
                throw new IllegalArgumentException("unprocessed request of value " + rest);
            }
        }
    }

    public void setNext(Dispenser next) {
        this.next = next;
    }
}
