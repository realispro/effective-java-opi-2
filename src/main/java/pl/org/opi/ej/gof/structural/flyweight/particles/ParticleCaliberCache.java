package pl.org.opi.ej.gof.structural.flyweight.particles;

import java.util.HashMap;
import java.util.Map;

public class ParticleCaliberCache {

    private static Map<Integer, ParticleCaliber> cache = new HashMap<>();

    public static ParticleCaliber getCaliber(int caliber){
        ParticleCaliber particleCaliber = cache.get(caliber);
        if(particleCaliber==null){
            System.out.println("[cache] creating caliber " + caliber);
            particleCaliber = new ParticleCaliber(caliber);
            cache.put(caliber, particleCaliber);
        } else {
            System.out.println("[cache] providing caliber " + caliber);
        }

        return particleCaliber;
    }
}
