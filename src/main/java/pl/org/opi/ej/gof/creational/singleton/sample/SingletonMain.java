package pl.org.opi.ej.gof.creational.singleton.sample;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonMain {

    public static void main(String[] args) {

        EnumSingleton instance1 = EnumSingleton.getInstance();
        EnumSingleton instance2 = null;//LazySingleton.getInstance();

        Class clazz = EnumSingleton.class;
        try {
            Constructor constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            Object o = constructor.newInstance();
            instance2 = (EnumSingleton) o;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("same? " + (instance1==instance2));

        instance1.doNothing();
        instance1.doNothing();

    }
}
