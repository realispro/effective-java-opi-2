package pl.org.opi.ej.gof.creational.factory.method.beer;

public enum BeerType {
    PILS,
    IPA,
    FRUITY
}
