package pl.org.opi.ej.gof.creational.factory.method.notification;

// product
public interface Notification {

    public void emit(String text);
}
