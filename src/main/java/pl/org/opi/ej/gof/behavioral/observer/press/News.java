package pl.org.opi.ej.gof.behavioral.observer.press;


public class News {

    private String headline;

    private String article;

    public News(String headline, String article) {
        this.headline = headline;
        this.article = article;
    }

    public String getHeadline() {
        return headline;
    }

    public String getArticle() {
        return article;
    }

    @Override
    public String toString() {
        return "News{" +
                "headline='" + headline + '\'' +
                ", article='" + article + '\'' +
                '}';
    }
}
