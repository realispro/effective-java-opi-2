package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.predator;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Bird;

public class Eagle implements Bird {

    @Override
    public void fly() {
        System.out.println("eagle is flying high");
    }
}
