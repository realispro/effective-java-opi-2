package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.AnimalProvider;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Bird;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Fish;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Mammal;

public class PrehistoricAnimalProvider implements AnimalProvider {
    @Override
    public Bird provideBird() {
        return new Archeopteryx();
    }

    @Override
    public Fish provideFish() {
        return new Ichtiozaur();
    }

    @Override
    public Mammal provideMammal() {
        return new Mammut();
    }
}
