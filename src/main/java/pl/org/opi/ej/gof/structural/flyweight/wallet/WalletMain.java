package pl.org.opi.ej.gof.structural.flyweight.wallet;

import java.util.ArrayList;
import java.util.List;

public class WalletMain {

    public static void main(String[] args) {
        System.out.println("let's put some coins into wallet");

        List<Coin> wallet = new ArrayList<>();
        wallet.add(CoinCache.getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getCoin(2, Currency.PLN));
        wallet.add(CoinCache.getCoin(5, Currency.DOLLAR));
        wallet.add(CoinCache.getCoin(2, Currency.EURO));
        wallet.add(CoinCache.getCoin(2, Currency.EURO));
        wallet.add(CoinCache.getCoin(1, Currency.PLN));
        wallet.add(CoinCache.getCoin(1, Currency.PLN));

        System.out.println("wallet content: " + wallet);



    }
}
