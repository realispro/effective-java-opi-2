package pl.org.opi.ej.gof.behavioral.cor.purchase;

public abstract class Approver {

    protected Approver next;

    private String name;

    public Approver(String name){
        this.name = name;
    }

    public abstract int getLimit();

    public final void approve(Purchase p){
        if(p.getAmount()<=getLimit()||getLimit()==-1) {
            System.out.println("Przyjęte do realizacji przez " + name);
            p.approve(name);
        } else if (this.next != null) {
            this.next.approve(p);
        }
    }

    public void setNext(Approver next) {
        this.next = next;
    }

    public String getName() {
        return name;
    }
}
