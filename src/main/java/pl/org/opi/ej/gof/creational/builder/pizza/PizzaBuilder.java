package pl.org.opi.ej.gof.creational.builder.pizza;

import java.util.ArrayList;
import java.util.List;

public class PizzaBuilder {

   /* private String dough;

    private List<String> toppings = new ArrayList<>();

    private String sause;*/


    private Pizza pizza = new Pizza();

    public PizzaBuilder setDough(String dough){
        pizza.setDough(dough);
        return this;
    }

    public PizzaBuilder addTopping(String topping){
        if(pizza.getDough()==null){
            throw new IllegalArgumentException("define dough first");
        }
        pizza.addTopping(topping);
        return this;
    }

    public PizzaBuilder setSause(String sause){
        pizza.setSause(sause);
        return this;
    }

    public Pizza bake(){
        if(pizza.getDough()==null){
            throw new IllegalStateException("dough is mandatory");
        }
        return pizza;
    }

}
