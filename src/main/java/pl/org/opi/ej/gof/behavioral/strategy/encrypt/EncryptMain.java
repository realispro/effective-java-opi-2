package pl.org.opi.ej.gof.behavioral.strategy.encrypt;


public class EncryptMain {

    public static void main(String[] args){
        Encrypter encrypter = new GADERYPOLUKIEncrypter();
        SafeStore store = new SafeStore(encrypter);
        boolean stored = store.store("LOREM IPSUM");
        System.out.println("safely stored? " + stored);
    }

}
