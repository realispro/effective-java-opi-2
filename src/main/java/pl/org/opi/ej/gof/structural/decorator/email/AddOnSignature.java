package pl.org.opi.ej.gof.structural.decorator.email;

public class AddOnSignature implements Email {

    private Email email;

    private String signature;

    public AddOnSignature(Email email, String signature) {
        this.email = email;
        this.signature = signature;
    }

    public String getTitle() {
        return email.getTitle();
    }

    public String getContent() {
        return email.getContent() + "\n" + signature;
    }
}
