package pl.org.opi.ej.gof.creational.singleton.sample;

public class LazySingleton {

    private static LazySingleton instance;

    private LazySingleton(){}

    public /*synchronized*/ static LazySingleton getInstance(){

        if(instance==null) {
            synchronized (LazySingleton.class) {
                if (instance == null) {
                    instance = new LazySingleton();
                }
            }
        }
        return instance;
    }

    public void doNothing(){
        System.out.println("doing nothing");
    }

}
