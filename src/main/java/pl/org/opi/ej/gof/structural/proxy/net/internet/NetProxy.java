package pl.org.opi.ej.gof.structural.proxy.net.internet;

import pl.org.opi.ej.gof.structural.proxy.net.Internet;

import java.util.ArrayList;
import java.util.List;

public class NetProxy implements Internet {

    private static List<String> blackList = new ArrayList<>();

    static {
        blackList.add("xxx");
        blackList.add("onet.pl");
        blackList.add("facebook.com");
    }

    private Internet internet;

    NetProxy(Internet internet){
        this.internet = internet;
    }

    @Override
    public void connectTo(String address) {
        blackList.forEach(a-> {
            if(address.trim().startsWith(a)){
                throw new RuntimeException("access denied to " + address);
            }
        });
        internet.connectTo(address);
    }
}
