package pl.org.opi.ej.gof.creational.factory.method.notification;

public class PigeonNotification implements Notification{
    @Override
    public void emit(String text) {
        System.out.println("pigeon carrying message: " + text);
    }
}
