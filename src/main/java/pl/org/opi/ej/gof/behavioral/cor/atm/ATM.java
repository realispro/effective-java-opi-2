package pl.org.opi.ej.gof.behavioral.cor.atm;

public class ATM {


    public ATM() {
        // TODO construct dispenser chain
    }

    public void withdraw(int amount){
        Request request = new Request(amount);
        Dispenser dispenser = getDispenser();
        dispenser.dispense(request);
    }

    private Dispenser getDispenser(){
        Dispenser dispenser100 = new Dispenser(100);
        Dispenser dispenser50 = new Dispenser(50);
        Dispenser dispenser20 = new Dispenser(20);
        dispenser100.setNext(dispenser50);
        dispenser50.setNext(dispenser20);
        return dispenser100;
    }





}
