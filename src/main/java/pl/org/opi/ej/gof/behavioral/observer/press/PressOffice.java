package pl.org.opi.ej.gof.behavioral.observer.press;

import java.util.HashSet;
import java.util.Set;

// Subject
public class PressOffice {

    private Set<NewsListener> listeners;

    public PressOffice() {
        listeners = new HashSet<>();
    }

    public void dayWork(){
        News n1 = new News("Mundial: Poland won!", "Polska mistrzem Polski");
        broadcast(n1);
        News n2 = new News("Chemitrails are fake", "Chemitrails doesn't exists");
        broadcast(n2);
    }

    private void broadcast(News n){
        listeners.stream().forEach(s -> s.pushNews(n));
    }

    public void addSubscriber(NewsListener listener) {
        listeners.add(listener);
    }


}
