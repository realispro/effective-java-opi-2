package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian.ItalianRestaurant;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish.PolishRestaurant;

public class RestaurantMain {

    public static void main(String[] args) {

        Restaurant restaurant = Restaurant.getInstance(RestaurantType.POLISH);
                // v1: new PolishRestaurant();
                /* v2: RestaurantProvider
                .getInstance(RestaurantType.POLISH)
                .getRestaurant();*/

        MainDish mainDish = restaurant.getMainDish();
        mainDish.eat();

        Dessert dessert = restaurant.getDessert();
        dessert.enjoy();
    }
}
