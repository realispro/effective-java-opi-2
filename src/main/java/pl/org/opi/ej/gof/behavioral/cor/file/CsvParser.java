package pl.org.opi.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CsvParser extends Parser{
    @Override
    public void process(File file) throws IOException {
        Files
                .lines(Paths.get(file.getAbsolutePath()))
                .forEach(l -> {
                    String[] parsedLine = l.split(";");
                    for (String cell : parsedLine) {
                        System.out.print("[" + cell + "]");
                    }
                    System.out.println();
                });
    }
    @Override
    public String getTypeFile() {
        return ".csv";
    }
}
