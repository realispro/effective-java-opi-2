package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Pandoro implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoying pandoro dessert");
    }
}
