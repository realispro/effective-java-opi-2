package pl.org.opi.ej.gof.structural.adapter.sim;

public class Device {

    private String simId;

    public void install(Sim sim){
        simId = sim.getId();
        System.out.println("sim installed:" + simId);
    }

}
