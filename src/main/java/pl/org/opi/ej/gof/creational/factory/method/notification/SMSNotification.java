package pl.org.opi.ej.gof.creational.factory.method.notification;

public class SMSNotification implements Notification{

    @Override
    public void emit(String text) {
        System.out.println("sending SMS: " + text);
    }
}
