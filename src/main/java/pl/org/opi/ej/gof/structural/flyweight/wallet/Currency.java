package pl.org.opi.ej.gof.structural.flyweight.wallet;

public enum Currency {

    PLN,
    EURO,
    DOLLAR
}
