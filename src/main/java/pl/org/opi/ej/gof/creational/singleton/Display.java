package pl.org.opi.ej.gof.creational.singleton;

public class Display {

    private static Display instance;

    private Display(){}

    public synchronized static Display getInstance(){
        if(instance == null) {
            instance = new Display();
        }
        return instance;
    }

    public void show(String text){
        System.out.println("[" + text + "]");
    }
}
