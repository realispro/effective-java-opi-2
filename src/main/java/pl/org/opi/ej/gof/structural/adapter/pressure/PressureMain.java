package pl.org.opi.ej.gof.structural.adapter.pressure;

import pl.org.opi.ej.gof.structural.adapter.pressure.bar.Bar2PascalAdapter;
import pl.org.opi.ej.gof.structural.adapter.pressure.bar.BarPressureProvider;

public class PressureMain {

    public static void main(String[] args) {
        System.out.println("let's measure a pressure !");

        PascalPressure pascalPressure = new Bar2PascalAdapter(new BarPressureProvider());
        Display display = new Display();
        display.showPresure(pascalPressure);


    }
}
