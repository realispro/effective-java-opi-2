package pl.org.opi.ej.gof.behavioral.strategy.encrypt;

import java.util.HashMap;
import java.util.Map;

public class GADERYPOLUKIEncrypter implements Encrypter{

    private static final Map<String, String> map = new HashMap<>();

    static {
        map.put("G", "A");
        map.put("D", "E");
        map.put("R", "Y");
        map.put("P", "O");
        map.put("L", "U");
        map.put("K", "I");
    }

    @Override
    public byte[] encrypt(String s) throws Exception {
        for(String key : map.keySet()){
            s = s.replaceAll(key, map.get(key));
        }
        return s.getBytes();
    }
}
