package pl.org.opi.ej.gof.behavioral.strategy.payment;

@FunctionalInterface
public interface PaymentMethod {

    void chargePay(double value);
}
