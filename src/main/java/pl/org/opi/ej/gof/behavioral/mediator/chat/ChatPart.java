package pl.org.opi.ej.gof.behavioral.mediator.chat;


public class ChatPart {

    private String name;

    public ChatPart(String name) {
        this.name = name;
    }

    public void receive(String message){
        System.out.println("[" + name + "] someone send a letter: " + message);
    }
}
