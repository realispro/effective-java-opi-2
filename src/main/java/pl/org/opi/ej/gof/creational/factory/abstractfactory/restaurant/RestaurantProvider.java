package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian.ItalianRestaurantProvider;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish.PolishRestaurantProvider;

public interface RestaurantProvider {

    Restaurant getRestaurant();

    static RestaurantProvider getInstance(RestaurantType restaurantType) {
        switch (restaurantType) {
            default:
            case POLISH:
                return new PolishRestaurantProvider();
            case ITALIAN:
                return new ItalianRestaurantProvider();
        }
    }
}
