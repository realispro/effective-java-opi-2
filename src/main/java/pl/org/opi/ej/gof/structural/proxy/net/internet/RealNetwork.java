package pl.org.opi.ej.gof.structural.proxy.net.internet;


import pl.org.opi.ej.gof.structural.proxy.net.Internet;

public class RealNetwork implements Internet {

    RealNetwork(){}

    @Override
    public void connectTo(String address) {
        System.out.println("connecting to " + address);
    }
}
