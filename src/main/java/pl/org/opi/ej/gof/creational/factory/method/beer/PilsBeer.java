package pl.org.opi.ej.gof.creational.factory.method.beer;

public class PilsBeer implements Beer {
    @Override
    public String getName() {
        return "plis beer";
    }
    @Override
    public float getVoltage() {
        return 5;
    }
}
