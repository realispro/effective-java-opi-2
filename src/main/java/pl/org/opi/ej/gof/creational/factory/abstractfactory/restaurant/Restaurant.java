package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.italian.ItalianRestaurant;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish.PolishRestaurant;

// abstract factory
public interface Restaurant {

    MainDish getMainDish();

    Dessert getDessert();

    static Restaurant getInstance(RestaurantType type){
        switch (type){
            case POLISH:
                return new PolishRestaurant();
            default:
                return new ItalianRestaurant();
        }
    }
}
