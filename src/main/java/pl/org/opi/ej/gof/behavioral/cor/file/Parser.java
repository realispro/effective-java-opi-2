package pl.org.opi.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public abstract class Parser {

    protected Parser next;

    public abstract void process(File file) throws IOException;

    public abstract String getTypeFile();

    public void parse(File file) throws IOException {
        if(file.getName().endsWith(getTypeFile())) {
            process(file);
        } else {
            if(this.next!=null) {
                this.next.parse(file);
            }
        }
    };
    public void setNext(Parser next) {
        this.next = next;
    }
}
