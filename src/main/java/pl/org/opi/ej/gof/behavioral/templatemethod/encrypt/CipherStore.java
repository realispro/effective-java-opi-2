package pl.org.opi.ej.gof.behavioral.templatemethod.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public abstract class CipherStore extends SafeStore {

    public abstract String getAlgorithm();

    public abstract byte[] getKeyValue();

    protected byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(getKeyValue(), getAlgorithm());
        Cipher c = Cipher.getInstance(getAlgorithm());
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
