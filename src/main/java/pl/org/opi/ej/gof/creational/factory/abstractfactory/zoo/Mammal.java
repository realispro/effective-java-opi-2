package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo;

public interface Mammal {

    void move();
}
