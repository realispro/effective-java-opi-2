package pl.org.opi.ej.gof.behavioral.templatemethod.encrypt;


public class EncryptMain {

    public static void main(String[] args){
        SafeStore store = new AESCipherStore();
        boolean stored = store.store("LOREM IPSUM");
        System.out.println("safely stored? " + stored);
    }

}
