package pl.org.opi.ej.gof.behavioral.strategy.payment;

public class ShoppingMain {

    public static void main(String[] args) {
        System.out.println("let's buy smth!");

        Shopping s = new Shopping();

        s.addItem(new Item("mleko", 3, 2.2));
        s.addItem(new Item("Ipa", 24, 3.5));
        s.addItem(new Item("paluszki", 2, 1.79));

        Paypal paypal = new Paypal("joe", "doe");
        CreditCard cc = new CreditCard("6534653", 233);

        s.pay(paypal);


    }
}