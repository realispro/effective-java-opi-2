package pl.org.opi.ej.gof.creational.factory.method.beer;

public class BeerFactory {

    public Beer createBeer(){
        return createBeer(BeerType.IPA);
    }

    public Beer createBeer(BeerType type){
        switch (type){
            case IPA:
                return new IpaBeer();
            case PILS:
                return new PilsBeer();
            default:
                return new FruityBeer();
        }
    }
}
