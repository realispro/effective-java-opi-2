package pl.org.opi.ej.gof.structural.proxy.image;

public class ImageFile implements Image{

    private byte[] data;

    public ImageFile(String fileName){
        System.out.println("loading data from " + fileName);
        data = fileName.getBytes(); // emulation of data loading from file
    }

    @Override
    public byte[] getData() {
        return data;
    }
}
