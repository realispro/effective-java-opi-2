package pl.org.opi.ej.gof.structural.decorator.email;

public class EmailMain {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email =
                new UrgencyDecorator(
                        new RODODecorator(
                                new AddOnSignature(
                                        new EmailMessage(
                                                "Poland vs Netherlands",
                                                "Will we win?"),
                                        "Regards,\nMarcin")
                        )
                );

        System.out.println("sending email:\ntitle=[" + email.getTitle() + "],\ncontent=[\n" + email.getContent() + "\n]");

    }

}
