package pl.org.opi.ej.gof.behavioral.cor.purchase;

public class PurchaseMain {

    public static void main(String[] args) {
        System.out.println("let's process a purchase");

        Purchase car = new Purchase(243162361, "samochod dla sprzedawcy");
        Purchase paper = new Purchase(40, "papier do drukarki");
        Purchase laptop = new Purchase(8000, "laptop");
        Purchase keyboard = new Purchase(800, "keyboard");

        Approver approver = getApprover();
        approver.approve(car);
        approver.approve(paper);
        approver.approve(laptop);
        approver.approve(keyboard);

        System.out.println("car = " + car);
        System.out.println("paper = " + paper);
        System.out.println("laptop = " + laptop);
        System.out.println("keyboard = " + keyboard);
    }

    private static Approver getApprover(){
        Secretary secretary = new Secretary("sekretariat");
        Director director = new Director("inz. Karwowski");
        President president = new President("Biden");
        secretary.setNext(director);
        director.setNext(president);
        return secretary;
    }
}
