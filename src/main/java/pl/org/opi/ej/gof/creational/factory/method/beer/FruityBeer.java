package pl.org.opi.ej.gof.creational.factory.method.beer;

public class FruityBeer implements Beer {
    @Override
    public String getName() {
        return "fruity beer";
    }
    @Override
    public float getVoltage() {
        return 2;
    }
}
