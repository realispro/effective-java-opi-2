package pl.org.opi.ej.gof.structural.proxy.image;

public class ImageProxy implements Image{

    private String fileName;

    private Image image;

    public ImageProxy(String fileName){
        this.fileName = fileName;
    }

    @Override
    public byte[] getData() {
        if(image==null) {
            image = new ImageFile(fileName);
        }
        return image.getData();
    }
}
