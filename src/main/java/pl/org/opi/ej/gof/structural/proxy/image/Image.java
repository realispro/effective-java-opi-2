package pl.org.opi.ej.gof.structural.proxy.image;

public interface Image {

    byte[] getData();

}
