package pl.org.opi.ej.gof.behavioral.observer.followers;

public interface Follower {

    void follow(Post post);

}
