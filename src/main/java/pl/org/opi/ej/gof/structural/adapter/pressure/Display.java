package pl.org.opi.ej.gof.structural.adapter.pressure;

public class Display {

    public void showPresure(PascalPressure pp){
        System.out.println("current pressure in Pascal: " + pp.getPressure());
    }
}
