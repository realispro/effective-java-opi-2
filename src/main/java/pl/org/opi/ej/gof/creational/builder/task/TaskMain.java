package pl.org.opi.ej.gof.creational.builder.task;

public class TaskMain {

    public static void main(String[] args) {
        
        Task task = new TaskBuilder()
                .setId(10)
                .setTitle("Trzeba to zrobić.")
                .setDescription("Trzeba to zrobić dobrze. ")
                .setPriority(5)
                .buildTask();

        System.out.println("task = " + task);
    }
    
}
