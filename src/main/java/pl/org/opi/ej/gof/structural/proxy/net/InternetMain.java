package pl.org.opi.ej.gof.structural.proxy.net;


import pl.org.opi.ej.gof.structural.proxy.net.internet.InternetProvider;
import pl.org.opi.ej.gof.structural.proxy.net.internet.NetProxy;

public class InternetMain {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = InternetProvider.provideInternet();

        internet.connectTo("wp.pl");
        internet.connectTo("facebook.com/events");

    }
}
