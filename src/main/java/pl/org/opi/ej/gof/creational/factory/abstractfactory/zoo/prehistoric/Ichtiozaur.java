package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Fish;

// ichtiozaur is not a fish!!!
// TODO change to Dunkleosteus
// https://en.wikipedia.org/wiki/Dunkleosteus
public class Ichtiozaur implements Fish {
    @Override
    public void swim() {
        System.out.println("ichtiozaur is swimming");
    }
}
