package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.polish;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Faworki implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoying faworki");
    }
}
