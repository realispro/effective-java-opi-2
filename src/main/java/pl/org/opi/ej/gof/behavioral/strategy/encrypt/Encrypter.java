package pl.org.opi.ej.gof.behavioral.strategy.encrypt;

// Strategy
@FunctionalInterface
public interface Encrypter {

    byte[] encrypt(String s) throws Exception;

}
