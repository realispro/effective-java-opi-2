package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.predator;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.Mammal;

public class Tiger implements Mammal {
    @Override
    public void move() {
        System.out.println("tiger is running quietly");
    }
}
