package pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo;

import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.predator.PredatorAnimalProvider;
import pl.org.opi.ej.gof.creational.factory.abstractfactory.zoo.prehistoric.PrehistoricAnimalProvider;

public class ZooMain {

    public static void main(String[] args) {

        AnimalProvider ap = AnimalProvider.getInstance(AnimalType.PREHISTORIC);

        Bird bird = ap.provideBird();
        bird.fly();

        Fish fish = ap.provideFish();
        fish.swim();

        Mammal mammal = ap.provideMammal();
        mammal.move();

    }
}
