package pl.org.opi.ej.gof.creational.factory.abstractfactory.restaurant;

public enum RestaurantType {

    POLISH,
    ITALIAN
}
