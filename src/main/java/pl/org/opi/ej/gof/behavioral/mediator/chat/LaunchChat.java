package pl.org.opi.ej.gof.behavioral.mediator.chat;

public class LaunchChat {

    public static void main(String[] args) {

        Channel channel = new Channel("#effective-java-opi-2");

        ChatPart cp1 = new ChatPart("part1");
        ChatPart cp2 = new ChatPart("part2");
        ChatPart cp3 = new ChatPart("part3");

        channel.register(cp1);
        channel.register(cp2);
        channel.register(cp3);

        channel.send("Hey man!", cp1);
        channel.send("How are you?", cp2);
        channel.send("Fine :)", cp1);
    }
}
