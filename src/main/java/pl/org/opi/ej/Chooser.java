package pl.org.opi.ej;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Chooser {

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12);
        int index = new Random(new Date().getTime()).nextInt(numbers.size());
        System.out.println("Lucky person: " + numbers.get(index));
    }
}
