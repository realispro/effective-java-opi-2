package pl.org.opi.ej.soccer.team;

import java.util.List;

public class TeamBuilder {

    private Team team = new Team();

    public TeamBuilder addName(String name){
        team.setName(name);
        return this;
    }

    public TeamBuilder addGoalKeeper(Player player){
        team.setGoalKeeper(player);
        return this;
    }

    public TeamBuilder addDefenders(List<Player> players){
        team.setDefenders(players);
        return this;
    }

    public TeamBuilder addMidfields(List<Player> players){
        team.setMidfields(players);
        return this;
    }

    public TeamBuilder addAttackers(List<Player> players){
        team.setAttackers(players);
        return this;
    }

    public Team build(){
        // TODO verify mandatory fields
        return team;
    }

}
