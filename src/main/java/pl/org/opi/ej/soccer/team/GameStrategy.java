package pl.org.opi.ej.soccer.team;

public interface GameStrategy {

    int getDefendersCount();

    int getMidfieldsCount();

    int getAttackersCount();

}
