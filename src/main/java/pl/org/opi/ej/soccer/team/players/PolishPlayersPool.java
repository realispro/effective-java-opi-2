package pl.org.opi.ej.soccer.team.players;


import pl.org.opi.ej.soccer.team.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolishPlayersPool implements PlayersPool {

    private static PolishPlayersPool instance = new PolishPlayersPool();

    private PolishPlayersPool(){}

    public static PolishPlayersPool getInstance() {
        return instance;
    }



    public List<Player> goalKeepers = new ArrayList<>(Arrays.asList(
            new Player("Wojciech", "Szczesny", 6),
            new Player("Lukasz", "Fabianski", 9),
            new Player("Artur", "Boruc", 15),
            new Player("Grzegorz", "Szamotulski", 28)
    ));

    public List<Player> defenders = new ArrayList<>(Arrays.asList(
            new Player("Michal", "Pazdan", 120),
            new Player("Thiago", "Cionek", 122),
            new Player("Jakub", "Wawrzyniak", 88),
            new Player("Kamil", "Glik", 12),
            new Player("Bartosz", "Salamon", 99),
            new Player("Lukasz", "Piszczek", 14),
            new Player("Artur", "Jedrzejczyk", 110),
            new Player("Arkadiusz", "Glowacki", 120),
            new Player("Jakub", "Rzezniczak", 130)
    ));

    public List<Player> midfields = new ArrayList<>(Arrays.asList(
            new Player("Krzysztof", "Maczynski", 120),
            new Player("Tomasz", "Jodlowiec", 122),
            new Player("Karol", "Linetty", 88),
            new Player("Grzegorz", "Krychowiak", 12),
            new Player("Kamil", "Grosicki", 99),
            new Player("Jakub", "Blaszczykowski", 14),
            new Player("Slawomir", "Peszko", 110),
            new Player("Piotr", "Zielinski", 120),
            new Player("Bartosz", "Kapustka", 130),
            new Player("Filip", "Starzynski", 130)
    ));

    public List<Player> attackers = new ArrayList<>(Arrays.asList(
            new Player("Arkadiusz", "Milik", 9),
            new Player("Robert", "Lewandowski", 2),
            new Player("Mariusz", "Stepinski", 54),
            new Player("Zbigniew", "Boniek", 12),
            new Player("Euzebiusz", "Smolarek", 19)
    ));


    @Override
    public List<Player> getGoalKeepers() {
        return this.goalKeepers;
    }

    @Override
    public List<Player> getDefenders() {
        return defenders;
    }

    @Override
    public List<Player> getMidfields() {
        return midfields;
    }

    @Override
    public List<Player> getAttackers() {
        return attackers;
    }
}
