package pl.org.opi.ej.soccer.match.handlers;

import pl.org.opi.ej.soccer.match.Event;
import pl.org.opi.ej.soccer.match.EventHandler;
import pl.org.opi.ej.soccer.match.EventType;

public class RedCardHandler extends EventHandler {

    @Override
    public void handleEvent(Event e) {
        if(e.type== EventType.RED_CARD){
            e.team.getDefenders().remove(e.player);
            e.team.getMidfields().remove(e.player);
            e.team.getAttackers().remove(e.player);
            if(e.player==e.team.getGoalKeeper()) {
                e.team.setGoalKeeper(null);
            }
        } else {
            super.handleEvent(e);
        }
    }
}
