package pl.org.opi.ej.soccer.team.players;



import pl.org.opi.ej.soccer.team.Player;

import java.util.List;

public interface PlayersPool {

    List<Player> getGoalKeepers();

    List<Player> getDefenders();

    List<Player> getMidfields();

    List<Player> getAttackers();


}
