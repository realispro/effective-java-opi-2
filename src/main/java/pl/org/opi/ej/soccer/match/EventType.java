package pl.org.opi.ej.soccer.match;

public enum EventType {
    GOAL,
    YELLOW_CARD,
    RED_CARD
}