package pl.org.opi.ej.soccer.team.strategy;

import pl.org.opi.ej.soccer.team.GameStrategy;

public class OffensiveStrategy implements GameStrategy {
    @Override
    public int getDefendersCount() {
        return 2;
    }

    @Override
    public int getMidfieldsCount() {
        return 5;
    }

    @Override
    public int getAttackersCount() {
        return 3;
    }
}
