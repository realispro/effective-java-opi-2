package pl.org.opi.ej.soccer.match;

public interface Subscriber {

    void notify(EventType type, String teamName, String playerName);

}
