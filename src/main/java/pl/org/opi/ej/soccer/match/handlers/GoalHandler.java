package pl.org.opi.ej.soccer.match.handlers;

import pl.org.opi.ej.soccer.match.Event;
import pl.org.opi.ej.soccer.match.EventHandler;
import pl.org.opi.ej.soccer.match.EventType;

public class GoalHandler extends EventHandler {

    @Override
    public void handleEvent(Event e) {
        if(e.type==EventType.GOAL){
            e.match.score(e.team);
        } else {
            super.handleEvent(e);
        }
    }
}
