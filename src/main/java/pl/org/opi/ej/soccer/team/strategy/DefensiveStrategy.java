package pl.org.opi.ej.soccer.team.strategy;

import pl.org.opi.ej.soccer.team.GameStrategy;

public class DefensiveStrategy implements GameStrategy {
    @Override
    public int getDefendersCount() {
        return 5;
    }

    @Override
    public int getMidfieldsCount() {
        return 4;
    }

    @Override
    public int getAttackersCount() {
        return 1;
    }
}
