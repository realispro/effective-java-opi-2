package pl.org.opi.ej.soccer.match;

import pl.org.opi.ej.soccer.match.handlers.GoalHandler;
import pl.org.opi.ej.soccer.match.handlers.RedCardHandler;
import pl.org.opi.ej.soccer.match.handlers.YellowCardHandler;

public abstract class EventHandler {

    private static EventHandler eventHandler;

    static {
        EventHandler redCard = new RedCardHandler();
        EventHandler yellowCard = new YellowCardHandler();
        eventHandler = new GoalHandler();
        eventHandler.setNext(redCard);
        redCard.setNext(yellowCard);

    }

    private EventHandler next;



    public void handleEvent(Event e){
        if(next!=null){
            next.handleEvent(e);
        }
    }

    public void setNext(EventHandler next) {
        this.next = next;
    }

    public static EventHandler getInstance(){
        return eventHandler;
    }
}
