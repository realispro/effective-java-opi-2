package pl.org.opi.ej.soccer.team;

import pl.org.opi.ej.soccer.team.players.Picker;
import pl.org.opi.ej.soccer.team.players.PlayersPool;

public class Coach {

    private PlayersPool pool;

    private String name;

    public Coach(String name, PlayersPool pool, GameStrategy strategy) {
        this.pool = new Picker(pool, strategy);
        this.name = name;
    }

    public Team constructTeam(GameStrategy strategy){

        TeamBuilder builder = new TeamBuilder();

        return builder.addName(name)
                .addGoalKeeper(pool.getGoalKeepers().stream().findFirst().orElseThrow())
                .addDefenders(pool.getDefenders())
                .addMidfields(pool.getMidfields())
                .addAttackers(pool.getAttackers())
                .build();

    }
}
