package pl.org.opi.ej.soccer;


import pl.org.opi.ej.soccer.match.Match;
import pl.org.opi.ej.soccer.team.Coach;
import pl.org.opi.ej.soccer.team.strategy.DefensiveStrategy;
import pl.org.opi.ej.soccer.team.strategy.OffensiveStrategy;
import pl.org.opi.ej.soccer.team.Team;
import pl.org.opi.ej.soccer.team.players.GermanPlayersPool;
import pl.org.opi.ej.soccer.team.players.PolishPlayersPool;

public class SoccerMain {

    public static void main(String[] args) {

        Coach brzeczek = new Coach("Polska", PolishPlayersPool.getInstance(), new OffensiveStrategy());
        Coach loew = new Coach("Niemcy", GermanPlayersPool.getInstance(), new DefensiveStrategy());

        Team polska = brzeczek.constructTeam(new OffensiveStrategy());
        Team niemcy = loew.constructTeam(new DefensiveStrategy());

        Match match = new Match(polska, niemcy);

        match.play();
        match.showResult();
    }
}
