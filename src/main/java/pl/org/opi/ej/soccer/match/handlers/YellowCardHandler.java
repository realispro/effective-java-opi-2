package pl.org.opi.ej.soccer.match.handlers;

import pl.org.opi.ej.soccer.match.Event;
import pl.org.opi.ej.soccer.match.EventHandler;
import pl.org.opi.ej.soccer.match.EventType;
import pl.org.opi.ej.soccer.team.Player;

import java.util.ArrayList;
import java.util.List;

public class YellowCardHandler extends EventHandler {

    private List<Player> fined = new ArrayList<>();

    @Override
    public void handleEvent(Event e) {
        if(e.type== EventType.YELLOW_CARD){
            if(fined.contains(e.player)){
                e.team.getDefenders().remove(e.player);
                e.team.getMidfields().remove(e.player);
                e.team.getAttackers().remove(e.player);
                if(e.player==e.team.getGoalKeeper()) {
                    e.team.setGoalKeeper(null);
                }
            } else {
                fined.add(e.player);
            }
        } else {
            super.handleEvent(e);
        }
    }
}
