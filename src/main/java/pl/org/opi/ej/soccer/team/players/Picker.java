package pl.org.opi.ej.soccer.team.players;

import pl.org.opi.ej.soccer.team.GameStrategy;
import pl.org.opi.ej.soccer.team.Player;

import java.util.List;
import java.util.stream.Collectors;


public class Picker implements PlayersPool{

    private PlayersPool players;

    private GameStrategy strategy;

    public Picker(PlayersPool players, GameStrategy strategy) {
        this.players = players;
        this.strategy = strategy;
    }


    private List<Player> findByRank(List<Player> players, int amount) {
        return players.stream()
                .sorted((p1, p2) -> p1.getRank() - p2.getRank())
                .limit(amount)
                .collect(Collectors.toList());
    }

    @Override
    public List<Player> getGoalKeepers() {
        return findByRank(players.getGoalKeepers(), 1);
    }

    @Override
    public List<Player> getDefenders() {
        return findByRank(players.getGoalKeepers(), strategy.getDefendersCount());
    }

    @Override
    public List<Player> getMidfields() {
        return findByRank(players.getGoalKeepers(), strategy.getMidfieldsCount());
    }

    @Override
    public List<Player> getAttackers() {
        return findByRank(players.getGoalKeepers(), strategy.getAttackersCount());
    }
}
