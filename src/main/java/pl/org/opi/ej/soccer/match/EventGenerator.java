package pl.org.opi.ej.soccer.match;




import pl.org.opi.ej.soccer.team.Player;
import pl.org.opi.ej.soccer.team.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class EventGenerator {

    public static Event generateEvent(Match m){
        EventType type = randomEventType();
        Team randomTeam = randomTeam(m.getHost(),m.getGuest());
        Player p = randomPlayer(randomTeam);
        return new Event(type, m, randomTeam, p);
    }

    private static EventType randomEventType(){
        return EventType.values()[
                ThreadLocalRandom.current().nextInt(EventType.values().length)];
    }

    private static Team randomTeam(Team team1, Team team2){
        return ThreadLocalRandom.current().nextBoolean() ? team1 : team2;
    }

    private static Player randomPlayer(Team t){
        List<Player> players = new ArrayList<>();
        players.addAll(t.getDefenders());
        players.addAll(t.getMidfields());
        players.addAll(t.getAttackers());
        players.add(t.getGoalKeeper());

        return players.get(
                ThreadLocalRandom.current().nextInt(players.size()));
    }

}
